<?php
/**
 * Eav component
 *
 * @author      Andrey Rapshtynskiy <andrey.bh@gmail.com>
 */
namespace Rapa\Eav\Model\ResourceModel;

use Magento\Eav\Model\Entity\AbstractEntity;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;
use Magento\Framework\DataObject;

abstract class AbstractModel extends AbstractEntity
{
    /**
     * @var int
     */
    protected $_storeId;

    /**
     * Returns store id
     *
     * @return int
     */
    public function getStoreId()
    {
        return $this->_storeId ?: 0;
    }

    /**
     * Set store id
     *
     * @param $value
     * @return $this
     */
    public function setStoreId($value)
    {
        $this->_storeId = (int) $value;

        return $this;
    }

    /**
     * Retrieve select object for loading entity attributes values
     *
     * @param   DataObject $object
     * @param   string $table
     * @return  \Magento\Framework\DB\Select
     */
    protected function _getLoadAttributesSelect($object, $table)
    {
        $select = $this->getConnection()->select()->from(
            $table,
            []
        )->where(
            $this->getEntityIdField() . ' =?',
            $object->getId()
        )->where(
            'store_id' . ' =?',
            $this->getStoreId()
        );

        return $select;
    }

    /**
     * Save entity attribute value
     *
     * Collect for mass save
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param AbstractAttribute $attribute
     * @param mixed $value
     * @return $this
     */
    protected function _saveAttribute($object, $attribute, $value)
    {
        $table = $attribute->getBackend()->getTable();
        if (!isset($this->_attributeValuesToSave[$table])) {
            $this->_attributeValuesToSave[$table] = [];
        }

        $entityIdField = $attribute->getBackend()->getEntityIdField();

        $data = [
            $entityIdField  => $object->getId(),
            'attribute_id'  => $attribute->getId(),
            'store_id'      => $this->getStoreId(),
            'value'         => $this->_prepareValueForSave($value, $attribute),
        ];

        if (!$this->getEntityTable() || $this->getEntityTable() == \Magento\Eav\Model\Entity::DEFAULT_ENTITY_TABLE) {
            $data['entity_type_id'] = $object->getEntityTypeId();
        }

        $this->_attributeValuesToSave[$table][] = $data;

        return $this;
    }

    /**
     * Save and delete collected attribute values
     *
     * @return $this
     */
    protected function _processAttributeValues()
    {
        $connection = $this->getConnection();
        foreach ($this->_attributeValuesToSave as $table => $data) {
            $connection->insertOnDuplicate($table, $data, ['value']);
        }

        foreach ($this->_attributeValuesToDelete as $table => $valueIds) {
            $connection->delete($table, ['value_id IN (?)' => $valueIds, 'store_id =?' => $this->getStoreId()]);
        }

        // reset data arrays
        $this->_attributeValuesToSave = [];
        $this->_attributeValuesToDelete = [];

        return $this;
    }
}