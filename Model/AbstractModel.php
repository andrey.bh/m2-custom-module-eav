<?php
/**
 * Eav component
 *
 * @author      Andrey Rapshtynskiy <andrey.bh@gmail.com>
 */
namespace Rapa\Eav\Model;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;

abstract class AbstractModel extends \Magento\Framework\Model\AbstractModel
{
    const STORE_ID  = 'store_id';

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * AbstractModel constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->_storeManager = $storeManager;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Returns store id
     *
     * @return int
     */
    public function getStoreId()
    {
        if ($this->hasData(self::STORE_ID)) {
            return $this->getData(self::STORE_ID);
        }
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * Set store id
     *
     * @param int $value
     * @return AbstractModel
     */
    public function setStoreId($value)
    {
        $this->getResource()->setStoreId($value);

        return $this->setData(self::STORE_ID, $value);
    }
}