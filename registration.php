<?php
/**
 * Eav component
 *
 * @author      Andrey Rapshtynskiy <andrey.bh@gmail.com>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Rapa_Eav',
    __DIR__
);