<?php
/**
 * Eav component
 *
 * @author      Andrey Rapshtynskiy <andrey.bh@gmail.com>
 */
namespace Rapa\Eav\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var SchemaSetupInterface
     */
    protected $_setup;

    /**
     * @var array
     */
    protected $_entityOptions  = [
        'datetime'  => [
            'type'      => Table::TYPE_DATETIME,
            'size'      => null,
            'options'   => [],
        ],
        'decimal'   => [
            'type'      => Table::TYPE_DECIMAL,
            'size'      => '12,4',
            'options'   => [],
        ],
        'int'       => [
            'type'      => Table::TYPE_INTEGER,
            'size'      => null,
            'options'   => [],
        ],
        'text'      => [
            'type'      => Table::TYPE_TEXT,
            'size'      => '64k',
            'options'   => [],
        ],
        'varchar'   => [
            'type'      => Table::TYPE_TEXT,
            'size'      => 255,
            'options'   => [],
        ],
    ];

    /**
     * Logic for installation process
     *
     * @return $this
     */
    protected function _installProcess()
    {
        // ToDo: The logic for installation should be added here

        return $this;
    }

    /**
     * Creating Entity Attribute Backend Table
     *
     * @param string $tableName
     * @param string $type
     * @param string|null $comment
     * @return $this
     */
    public function createEntityTable($tableName, $type, $comment = null)
    {
        $_tableName = $tableName . '_' . $type;
        $options = $this->getEntityOptions($type);
        if (!empty($options)) {
            $table = $this->_setup->getConnection(
            )->newTable(
                $this->_setup->getTable($_tableName)
            )->addColumn(
                'value_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Value ID'
            )->addColumn(
                'attribute_id',
                Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Attribute ID'
            )->addColumn(
                'store_id',
                Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Store ID'
            )->addColumn(
                'entity_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Entity ID'
            )->addColumn(
                'value',
                $options['type'],
                $options['size'],
                $options['options'],
                'Value'
            )->addIndex(
                $this->_setup->getIdxName(
                    $_tableName,
                    ['entity_id', 'attribute_id', 'store_id'],
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['entity_id', 'attribute_id', 'store_id'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            )->addIndex(
                $this->_setup->getIdxName(
                    $_tableName,
                    ['attribute_id']
                ),
                ['attribute_id']
            )->addIndex(
                $this->_setup->getIdxName(
                    $_tableName,
                    ['store_id']
                ),
                ['store_id']
            )->addForeignKey(
                $this->_setup->getFkName(
                    $_tableName,
                    'attribute_id',
                    'eav_attribute',
                    'attribute_id'
                ),
                'attribute_id',
                $this->_setup->getTable('eav_attribute'),
                'attribute_id',
                Table::ACTION_CASCADE
            )->addForeignKey(
                $this->_setup->getFkName(
                    $_tableName,
                    'entity_id',
                    $tableName,
                    'entity_id'
                ),
                'entity_id',
                $this->_setup->getTable($tableName),
                'entity_id',
                Table::ACTION_CASCADE
            )->addForeignKey(
                $this->_setup->getFkName(
                    $_tableName,
                    'store_id',
                    'store',
                    'store_id'
                ),
                'store_id',
                $this->_setup->getTable('store'),
                'store_id',
                Table::ACTION_CASCADE
            )->setComment((!empty($comment) ? $comment . ' ' : '') . ucfirst($type) . ' Attribute Backend Table');
            $this->_setup->getConnection()->createTable($table);
        }

        return $this;
    }

    /**
     * Creating table if not exist
     *
     * @param string $tableName
     * @param array $columns
     * @param string|null $comment
     * @return $this
     */
    public function createTable($tableName, $columns = [], $comment = null)
    {
        if (!$this->_setup->tableExists($tableName)) {
            $this->_setup->getConnection()->createTable(
                $this->prepareTable($tableName, $columns, $comment)
            );
        }

        return $this;
    }

    /**
     * Returns entity options
     *
     * @param string $type
     * @return array|mixed
     */
    public function getEntityOptions($type)
    {
        return (isset($this->_entityOptions[$type]) ? $this->_entityOptions[$type] : []);
    }

    /**
     * Preparing table
     *
     * @param string $tableName
     * @param array $columns
     * @param string|null $comment
     * @return Table
     */
    public function prepareTable($tableName, $columns = [], $comment = null)
    {
        $table = $this->_setup->getConnection(
        )->newTable(
            $this->_setup->getTable($tableName)
        )->addColumn(
            'entity_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Entity ID'
        );

        foreach ($columns as $column) {
            if (isset($column['name']) && isset($column['type'])) {
                $table->addColumn(
                    $column['name'],
                    $column['type'],
                    (isset($column['size']) ? $column['size'] : null),
                    (isset($column['options']) ? $column['options'] : []),
                    (isset($column['comment']) ? $column['comment'] : null)
                );
            }
        }

        if (!empty($comment)) {
            $table->setComment($comment . ' Table');
        }

        return $table;
    }

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $this->_setup = $setup;
        $setup->startSetup();
        $this->_installProcess();
        $setup->endSetup();
    }
}